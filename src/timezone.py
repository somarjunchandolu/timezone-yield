import requests
import re
import pandas as pd
import logging

# logging setup
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s :: %(levelname)s :: %(message)s', filename='app.log', filemode='a')
console = logging.StreamHandler()
console.setLevel(logging.INFO)

# function for getting data from a given url
def getData(url):
    # handling fetch errors
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return response
        else:
            logging.error(response)
            return None
    except Exception as e:
        logging.error(e)

# processing the timezones data
def processData(response):
    if response == None:
        return None
    else:
        data = list(re.findall(r'"(.*?)"', response.text))
        # mapping data and finding if strings in data has '/' and raising an exception
        data = list(map(lambda x: x if '/' in x else logging.warning('Exception TZ: '+x+ ' has one field'), data))
        # filtering empty strings
        data = list(filter(None, data))
        # splitting data with delimeter '/'
        data = list(map(lambda x: x.split('/'), data))
        # mapping data and finding if strings in data has more than 2-'/'s and raising an exception
        data = list(map(lambda x: x if len(x) == 2 else logging.warning('Exception TZ: ['+str(x).strip('[]')+'] has more than two fields'), data))
        # filtering empty strings
        data = list(filter(None, data))
        finalData = pd.DataFrame(data, columns=['Region', 'City'])
        # pusing data into dataframes
        finalData = finalData.groupby('Region')['City'].apply(list).reset_index(name='Cities')
        pd.set_option('max_colwidth', -1)
        # printing final results
        logging.info('Timezones')
        logging.info(finalData)
        return finalData

# calling function
data = getData('http://worldtimeapi.org/api/timezone')
processData(data)


