import unittest
from timezone import getData, processData

class testTimeZone(unittest.TestCase):
    # checking successful API call
    def testGetResponse(self):
        response = getData('http://worldtimeapi.org/api/timezone')
        self.assertEquals(response.status_code, 200)
    
    # checking if API returned object data
    def testIsObject(self):
        response = getData('http://worldtimeapi.org/api/timezone')
        self.assertIsInstance(response.text, object)
    
    # checking if data frame has result
    def testDataProcessed(self):
        response = getData('http://worldtimeapi.org/api/timezone')
        finalData = processData(response)
        self.assertGreater(len(finalData),1)
        self.assertEqual(len(finalData), 10)
if __name__ == '__main__':
    unittest.main()